package edu.utep.cs.cs3331.sudoku;

public class Main {
	
	private ConsoleUI ui = new ConsoleUI();
	private Board board;
	
	public static void main(String [] args) {
		new Main().play();
	}
	
	private void play() {
		ui.welcome();
		int size = ui.askSize();
		board = new Board(size);
		while (!board.isSolved()) {
			
			int x = ui.xcoordinates();
			int y = ui.ycoordinates();
			int value = ui.value();
			
			
			
			board.update(size,x,y,value);
			
			
			
			
			
		}
		
		ui.showMessage("Solved!");
	}

}
