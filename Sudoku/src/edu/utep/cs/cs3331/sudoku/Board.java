package edu.utep.cs.cs3331.sudoku;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class Board {
	
	int[][] array;
	
	
	private Board create; 
	
	public int size;
	
	private InputStream in;
	private PrintStream out;

	public Board(int size) {
		this.size = size;
		this.in = System.in;
		this.out = System.out;
		Grid(size);
		this.array = new int[size][size];
	
	}



	public boolean isSolved() {
		// TODO Auto-generated method stub
		return false;
	}

public void update(int size, int x, int y, int value) {
	
	this.array[x][y]= value;
	
	Grid(size, value, x, y);
	
	
	//System.out.println(Arrays.deepToString(array));
	
		
	
	}



private void Grid(int size, int value, int x, int y) {
	
	
	
	for( int i = 0; i<(size*2)+1;i++) {
		for(int j = 0; j<(size*2)+1; j++) {
			
			if(i%2==0) {
				if (j%2==0) {
					if  (i==Math.sqrt(size)*Math.sqrt(size)) {
						if (j>= Math.sqrt(size)&&j<Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size)) {
							System.out.print("*");
						}
						
						else {
						
						System.out.print("+");
						}
					}
					else if (j==Math.sqrt(size)*2&& (i!=0&&i!=Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size))) {
						
						System.out.print("*");
						
					}
					
				
			
					else if (size >4) {
						if (j==Math.sqrt(size)*4&& (i!=0&&i!=Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size))) {
							
							System.out.print("*");
							
						}
						
						else if  (i==Math.sqrt(size)*2) {
							System.out.print("*");
						}
						else if (i==Math.sqrt(size)*4) {
							System.out.print("*");
						}
						else {
							System.out.print("+");
							}
					}
					else {
				System.out.print("+");
				}
				
			}
				if (j%2 ==1) {
					if  (i==Math.sqrt(size)*2) {
						System.out.print("===");
						
					}
					else if (size >4) {
						if  (i==Math.sqrt(size)*4) {
							System.out.print("===");
						}
					else {
					System.out.print("---");
				}
			}
					else {
						System.out.print("---");
					
			}
				}	
			}
				
			if (i%2==1) {
				if (j%2==0) {
					if  (j==Math.sqrt(size)*2&&i%2==1) {
						System.out.print("!");
						
					}
					else if (size >4) {
						if  (j==Math.sqrt(size)*4&&i%2==1) {
							System.out.print("!");
						}
						else {
							System.out.print("|");
							}
					}
					else {
						System.out.print("|");
						}
				}
				if (j%2==1) {

					int q;
					
					for( int a = 0; a<size;a++) {
						for(int b = 0; b<size; b++) {
							
							q = array[a][b];
							
							if(q>0) {
								
								
								if ((a*2)+1 ==j && (b*2)+1==i) {
									
									System.out.print(" ");
									System.out.print(q);
									System.out.print(" ");
							}
								
								
								
							}
							else if (q==0 && (a*2)+1 ==j && (b*2)+1==i) {
								System.out.print("   ");
								
								
							}
							
							
							
						}
						

						
					
					
					}
					
					
					
						}
				
			
			
			}
		}
		
		System.out.println();
		
	}
		
	
}

private void Grid(int size) {
	
	
	for( int i = 0; i<(size*2)+1;i++) {
		for(int j = 0; j<(size*2)+1; j++) {
			
			if(i%2==0) {
				if (j%2==0) {
					if  (i==Math.sqrt(size)*Math.sqrt(size)) {
						if (j>= Math.sqrt(size)&&j<Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size)) {
							System.out.print("*");
						}
						
						else {
						
						System.out.print("+");
						}
					}
					else if (j==Math.sqrt(size)*2&& (i!=0&&i!=Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size))) {
						
						System.out.print("*");
						
					}
					
				
			
					else if (size >4) {
						if (j==Math.sqrt(size)*4&& (i!=0&&i!=Math.sqrt(size)*Math.sqrt(size)*Math.sqrt(size))) {
							
							System.out.print("*");
							
						}
						
						else if  (i==Math.sqrt(size)*2) {
							System.out.print("*");
						}
						else if (i==Math.sqrt(size)*4) {
							System.out.print("*");
						}
						else {
							System.out.print("+");
							}
					}
					else {
				System.out.print("+");
				}
				
			}
				if (j%2 ==1) {
					if  (i==Math.sqrt(size)*2) {
						System.out.print("===");
						
					}
					else if (size >4) {
						if  (i==Math.sqrt(size)*4) {
							System.out.print("===");
						}
					else {
					System.out.print("---");
				}
			}
					else {
						System.out.print("---");
					
			}
				}	
			}
				
			if (i%2==1) {
				if (j%2==0) {
					if  (j==Math.sqrt(size)*2&&i%2==1) {
						System.out.print("!");
						
					}
					else if (size >4) {
						if  (j==Math.sqrt(size)*4&&i%2==1) {
							System.out.print("!");
						}
						else {
							System.out.print("|");
							}
					}
					else {
						System.out.print("|");
						}
				}
				if (j%2==1) {
					System.out.print(" ");
					System.out.print(" ");
					System.out.print(" ");
						}
			
			
			}
		}
		
		System.out.println();
		
	}
		
		
	}	
	

}
