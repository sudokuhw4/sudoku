package edu.utep.cs.cs3331;

import java.util.Scanner;
import java.io.InputStream;
import java.io.PrintStream;

public class ConsoleUI {
	
	private InputStream in;
	private PrintStream out;
	
	public ConsoleUI() {

		in = System.in;
		out = System.out;
	}
	
	
	public ConsoleUI(InputStream i, PrintStream o) {
		
		i = System.in;
		o = System.out;
	}
	
	
	public void welcome() {
		
		
		out.println("Welcome to Sudoku!");
		
	}

	public int askSize() {
		out.println("press '1' for 4x4 or press '2' for 9x9");
		 Scanner sc = new Scanner(in);
	     int i = sc.nextInt();
	     
	     while (i != 1 && i != 2 ) {
    		 out.println("Sorry , that was an invalid input, please press 1 or 2");
    		  i = sc.nextInt();
    		 }
	     sc.close();
	     
	     if (i == 1) {
	    	 return 4;
	     } else{
	    	 
	    	 return 9;
	     }
	
		     
	}

	public void showMessage(String msg) {
		out.println(msg);
		
	}
	
	public int xcoordinates() {
		out.println("Plug in the coordinates for horizontal");
		Scanner sc = new Scanner(in);
	     int i = sc.nextInt();
	     sc.close();
	     return i;
		
	}
	
	public int ycoordinates() {
		out.println("Plug in the coordinates for vertical");
		Scanner sc = new Scanner(in);
	     int i = sc.nextInt();
	     sc.close();
	     return i;
		
	}
	
	public int value() {
		out.println("Plug in the value for the coordinate");
		Scanner sc = new Scanner(in);
	     int i = sc.nextInt();
	     sc.close();
	     return i;
		
	}
	
	
	
	

}
