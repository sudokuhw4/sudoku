package sudoku.model;

public class Solver extends Board
{
	String [][] view = new String[size][size];
	public Solver(int size) 
	{
		super(size);
	}
	public void solve(int i, int j) throws Exception
    {
       // if(i>8)
        //{
           //button.setEnabled(true);
        //}
        //not empty board-------------------------------------------------------------------
        if(array[i][j]!=0)
        {
            next(i,j);
        }
        //empty board-----------------------------------------------------------------------
        else
        {
            for(int num=1;num<size+1;num++)
            {
                if(checkRow(i,j,num) && checkCol(i,j,num) && checkBox(i,j,num))
                {
                    array[i][j]=num;
                    XView();
                    next(i,j);
                }
            }
            array[i][j]=0;
            XView();
        }
    }
    public void next(int i, int j) throws Exception
    {
        if(j<size-1)
        {
            solve(i, j+1);
        }
        else
        {
            solve(i+1,0);
        }
    }
    public void XView()
    {
        for(int i=0;i<size;i++)
        {
            for(int j=0;j<size;j++)
            {
                if(array[i][j]!=0)
                {
                    view[i][j] = (String.valueOf(array[i][j]));
                }
                else
                {
                    view[i][j] = "";
                }
            }
        }
    }
}
